﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class GradeForm : Form
    {
        StudentForm parrentForm;
        Grade grade;
        GradeModel gModel = new GradeModel();
        List<string> subjects = new List<string>();
        List<string> types = new List<string>();
        
        public GradeForm(Grade grade, StudentForm form) {            
            parrentForm = form;
            this.grade = grade;
            types = CreateTypes();
            subjects = CreateSubjects();

            InitializeComponent();
            LoadData();            
        }

        private List<string> CreateTypes()
        {
            List<string> types = new List<string>();

            types.Add("N");
            types.Add("V");

            return types;
        }

        private List<string> CreateSubjects()
        {
            List<string> subjects = new List<string>();

            subjects.Add("AJ");
            subjects.Add("CJ");
            subjects.Add("EKO");
            subjects.Add("DBS");
            subjects.Add("NJ");
            subjects.Add("OBN");
            subjects.Add("PVY");
            subjects.Add("TGR");
            subjects.Add("TEV");
            subjects.Add("PRG");
            subjects.Add("POS");
            subjects.Add("MAT");
            subjects.Add("FYZ");

            return subjects;
        }

        public void LoadData() {
            textBox_value.Text = grade.Value.ToString();
            dateTimePicker1.Value = grade.Date;
            comboBox_subject.Text = grade.Subject;
            textBox_theme.Text = grade.Theme;
            comboBox_type.Text = grade.Type;

            foreach (var item in subjects)
            {
                comboBox_subject.Items.Add(item);
            }

            foreach (var item in types)
            {
                comboBox_type.Items.Add(item);
            }
        }

        private void GradeForm_FormClosing(object sender, FormClosingEventArgs e) {
            parrentForm.Show();
        }

        private void button1_Click(object sender, EventArgs e) {
            grade.Value = int.Parse(textBox_value.Text);
            grade.Date = dateTimePicker1.Value;
            grade.Subject = comboBox_subject.Text;
            grade.Theme = textBox_theme.Text;
            grade.Type = comboBox_type.Text;

            parrentForm.ReloadGrades();

            this.Close();
        }

        private void button2_Click(object sender, EventArgs e) {
            gModel.Delete(grade);

            this.Close();
        }
    }
}
