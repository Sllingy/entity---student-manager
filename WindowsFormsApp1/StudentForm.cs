﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class StudentForm : Form
    {
        Student student;
        StudentGrid parrentForm;
        GradeModel gModel = new GradeModel();
        StudentModel sModel = new StudentModel();

        public StudentForm(StudentGrid form, Student student)
        {
            parrentForm = form;
            this.student = student;            

            InitializeComponent();
            this.LoadData();
        }                

        public void LoadData()
        {
            List<string> FOS = new List<string>();
            FOS.Add("Programovani");
            FOS.Add("Site");
            FOS.Add("Weby");
            foreach (var item in FOS)
            {
                comboBox_FOS.Items.Add(item);
            }

            textBox_id.Text = student.Id.ToString();
            textBox_name.Text = student.Name;
            textBox_sname.Text = student.Surname;
            textBox_year.Text = student.Year.ToString();
            dateTimePicker1.Value = student.DateOfBirth;
            textBox_PIN.Text = student.Pin;
            comboBox_FOS.Text = student.FieldOfStudy;            

            AddRows();
        }

        private void AddRows() {
            dataGridView1.Rows.Clear();
               
            if(student.Grades != null) {
                foreach (Grade grade in student.Grades) {
                    object[] row = { grade.Value, grade.Date, grade.Subject };
                    this.dataGridView1.Rows.Add(row);
                }
            }            
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e) {            
            parrentForm.Show();
        }

        private void button1_Click(object sender, EventArgs e) {            
            student.Name = textBox_name.Text;
            student.Surname = textBox_sname.Text;
            student.Year = int.Parse(textBox_year.Text);
            student.DateOfBirth = dateTimePicker1.Value;
            student.Pin = textBox_PIN.Text;
            student.FieldOfStudy = comboBox_FOS.Text;
            
            parrentForm.ReloadGrid();

            this.Close();
        }

        public void ReloadGrades() {
            AddRows();
        }

        private void button2_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e) {
            this.Hide();
            GradeForm form = new GradeForm(student.Grades[dataGridView1.CurrentCell.RowIndex], this);
            form.ShowDialog();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Hide();

            gModel.Create(new Grade {Date = DateTime.Now, StudentId = student.Id });            
            GradeForm form = new GradeForm(gModel.GetGradeLast(), this);
            form.ShowDialog();
        }

        private void button_DeleteGrade_Click(object sender, EventArgs e) {
            if (student.Grades != null) {
                gModel.Delete(student.Grades[dataGridView1.CurrentCell.RowIndex]);
            }
        }
    }
}
