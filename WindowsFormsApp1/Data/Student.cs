﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    public class Student
    {
        public int Id { get; set; }

        public string Name { get; set; } = "";

        public string Surname { get; set; } = "";

        public DateTime DateOfBirth { get; set; } = DateTime.Now;

        public string Pin { get; set; } = "";

        public string FieldOfStudy { get; set; } = "";

        public int Year { get; set; } = DateTime.Now.Year;
                
        public virtual List<Grade> Grades { get; set; }
    }
}
