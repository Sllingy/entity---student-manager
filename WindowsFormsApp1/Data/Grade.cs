﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    public class Grade
    {
        public int Id { get; set; }

        public int Value { get; set; } = 5;        

        public DateTime Date { get; set; } = DateTime.Now;

        public string Subject { get; set; } = "";

        public string Theme { get; set; } = "";

        public string Type { get; set; } = "";

        public int StudentId { get; set; }
    }
}
