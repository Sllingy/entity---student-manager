﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class StudentGrid : Form
    {
        StudentModel sModel = new StudentModel();
        GradeModel gModel = new GradeModel();

        public StudentGrid() {
            InitializeComponent();            
        }

        private void Form2_Load(object sender, EventArgs e) {
            AddRows();
        }        

        private void AddRows() {
            dataGridView2.Rows.Clear();
            List<Student> students = sModel.GetStudents();
            foreach (var student in students) {
                string[] row = { student.Id.ToString(), student.Name, student.Surname, student.DateOfBirth.ToString()};
                this.dataGridView2.Rows.Add(row);
            }
        }

        private void dataGridView2_CellContentClick(object sender, DataGridViewCellEventArgs e) {
            this.Hide();            
            StudentForm form = new StudentForm(this, sModel.GetStudents()[dataGridView2.CurrentCell.RowIndex]);
            form.ShowDialog();            
        }

        private void Form2_Enter(object sender, EventArgs e) {
            
        }

        public void ReloadGrid() {            
            AddRows();
        }

        private void Form2_FormClosing(object sender, FormClosingEventArgs e) {
            sModel.SaveChanges();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();

            sModel.Create(new Student { Name = "", Surname = "", DateOfBirth = DateTime.Now, Pin = "", FieldOfStudy = "", Year = DateTime.Now.Year });

            StudentForm form = new StudentForm(this, sModel.GetStudentLast());
            form.ShowDialog();
        }

        private void button_StudentDelete_Click(object sender, EventArgs e) {
            Student student = sModel.GetStudents()[dataGridView2.CurrentCell.RowIndex];
            //foreach (Grade grade in student.Grades) {
            //    gModel.Delete(grade);
            //}
            sModel.Delete(student);

            sModel.SaveChanges();
            ReloadGrid();
        }
    }
}
