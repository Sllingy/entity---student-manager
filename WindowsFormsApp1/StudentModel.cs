﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    class StudentModel
    {
        private StudentContext context = new StudentContext();

        public List<Student> GetStudents()
        {
            return context.Students.ToList();
        }

        public void Create(Student student) {
            context.Students.Add(student);
            context.SaveChanges();
        }       

        public Student GetStudentById(int id) {
            return context.Students.Find(id);
        }

        public Student GetStudentLast() {
            return context.Students.ToList().Last();
        }

        public void SaveChanges() {
            context.SaveChanges();
        }

        public void Delete(Student student) {
            context.Students.Remove(student);
        }        
    }
}
