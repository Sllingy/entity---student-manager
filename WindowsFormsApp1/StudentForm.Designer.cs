﻿namespace WindowsFormsApp1
{
    partial class StudentForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label_name = new System.Windows.Forms.Label();
            this.label_id = new System.Windows.Forms.Label();
            this.label_birth = new System.Windows.Forms.Label();
            this.label_sname = new System.Windows.Forms.Label();
            this.label_PIN = new System.Windows.Forms.Label();
            this.label_FOS = new System.Windows.Forms.Label();
            this.label_year = new System.Windows.Forms.Label();
            this.textBox_id = new System.Windows.Forms.TextBox();
            this.textBox_name = new System.Windows.Forms.TextBox();
            this.textBox_sname = new System.Windows.Forms.TextBox();
            this.textBox_PIN = new System.Windows.Forms.TextBox();
            this.textBox_year = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.clm_grade = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clm_ate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clm_subject = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.button3 = new System.Windows.Forms.Button();
            this.comboBox_FOS = new System.Windows.Forms.ComboBox();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.button_DeleteGrade = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // label_name
            // 
            this.label_name.AutoSize = true;
            this.label_name.Location = new System.Drawing.Point(12, 42);
            this.label_name.Name = "label_name";
            this.label_name.Size = new System.Drawing.Size(38, 13);
            this.label_name.TabIndex = 0;
            this.label_name.Text = "Jmeno";
            // 
            // label_id
            // 
            this.label_id.AutoSize = true;
            this.label_id.Location = new System.Drawing.Point(12, 16);
            this.label_id.Name = "label_id";
            this.label_id.Size = new System.Drawing.Size(18, 13);
            this.label_id.TabIndex = 1;
            this.label_id.Text = "ID";
            // 
            // label_birth
            // 
            this.label_birth.AutoSize = true;
            this.label_birth.Location = new System.Drawing.Point(12, 94);
            this.label_birth.Name = "label_birth";
            this.label_birth.Size = new System.Drawing.Size(83, 13);
            this.label_birth.TabIndex = 2;
            this.label_birth.Text = "Datum Narozeni";
            // 
            // label_sname
            // 
            this.label_sname.AutoSize = true;
            this.label_sname.Location = new System.Drawing.Point(12, 68);
            this.label_sname.Name = "label_sname";
            this.label_sname.Size = new System.Drawing.Size(43, 13);
            this.label_sname.TabIndex = 3;
            this.label_sname.Text = "Prijmeni";
            // 
            // label_PIN
            // 
            this.label_PIN.AutoSize = true;
            this.label_PIN.Location = new System.Drawing.Point(12, 120);
            this.label_PIN.Name = "label_PIN";
            this.label_PIN.Size = new System.Drawing.Size(63, 13);
            this.label_PIN.TabIndex = 4;
            this.label_PIN.Text = "Rodne cislo";
            // 
            // label_FOS
            // 
            this.label_FOS.AutoSize = true;
            this.label_FOS.Location = new System.Drawing.Point(12, 142);
            this.label_FOS.Name = "label_FOS";
            this.label_FOS.Size = new System.Drawing.Size(67, 13);
            this.label_FOS.TabIndex = 5;
            this.label_FOS.Text = "Studijni Obor";
            // 
            // label_year
            // 
            this.label_year.AutoSize = true;
            this.label_year.Location = new System.Drawing.Point(12, 172);
            this.label_year.Name = "label_year";
            this.label_year.Size = new System.Drawing.Size(41, 13);
            this.label_year.TabIndex = 6;
            this.label_year.Text = "Rocnik";
            // 
            // textBox_id
            // 
            this.textBox_id.Location = new System.Drawing.Point(101, 9);
            this.textBox_id.Name = "textBox_id";
            this.textBox_id.Size = new System.Drawing.Size(265, 20);
            this.textBox_id.TabIndex = 7;
            // 
            // textBox_name
            // 
            this.textBox_name.Location = new System.Drawing.Point(101, 35);
            this.textBox_name.Name = "textBox_name";
            this.textBox_name.Size = new System.Drawing.Size(265, 20);
            this.textBox_name.TabIndex = 8;
            // 
            // textBox_sname
            // 
            this.textBox_sname.Location = new System.Drawing.Point(101, 61);
            this.textBox_sname.Name = "textBox_sname";
            this.textBox_sname.Size = new System.Drawing.Size(265, 20);
            this.textBox_sname.TabIndex = 9;
            // 
            // textBox_PIN
            // 
            this.textBox_PIN.Location = new System.Drawing.Point(101, 113);
            this.textBox_PIN.Name = "textBox_PIN";
            this.textBox_PIN.Size = new System.Drawing.Size(265, 20);
            this.textBox_PIN.TabIndex = 11;
            // 
            // textBox_year
            // 
            this.textBox_year.Location = new System.Drawing.Point(101, 165);
            this.textBox_year.Name = "textBox_year";
            this.textBox_year.Size = new System.Drawing.Size(265, 20);
            this.textBox_year.TabIndex = 13;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(101, 191);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 14;
            this.button1.Text = "OK";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(291, 191);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 15;
            this.button2.Text = "Cancel";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.clm_grade,
            this.clm_ate,
            this.clm_subject});
            this.dataGridView1.Location = new System.Drawing.Point(372, 9);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(344, 150);
            this.dataGridView1.TabIndex = 16;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // clm_grade
            // 
            this.clm_grade.HeaderText = "Známka";
            this.clm_grade.Name = "clm_grade";
            this.clm_grade.ReadOnly = true;
            // 
            // clm_ate
            // 
            this.clm_ate.HeaderText = "Datum";
            this.clm_ate.Name = "clm_ate";
            this.clm_ate.ReadOnly = true;
            // 
            // clm_subject
            // 
            this.clm_subject.HeaderText = "Předmět";
            this.clm_subject.Name = "clm_subject";
            this.clm_subject.ReadOnly = true;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(603, 162);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(113, 23);
            this.button3.TabIndex = 17;
            this.button3.Text = "Pridat Známku";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // comboBox_FOS
            // 
            this.comboBox_FOS.FormattingEnabled = true;
            this.comboBox_FOS.Location = new System.Drawing.Point(101, 138);
            this.comboBox_FOS.Name = "comboBox_FOS";
            this.comboBox_FOS.Size = new System.Drawing.Size(265, 21);
            this.comboBox_FOS.TabIndex = 18;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(101, 87);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(265, 20);
            this.dateTimePicker1.TabIndex = 19;
            // 
            // button_DeleteGrade
            // 
            this.button_DeleteGrade.Location = new System.Drawing.Point(372, 165);
            this.button_DeleteGrade.Name = "button_DeleteGrade";
            this.button_DeleteGrade.Size = new System.Drawing.Size(113, 23);
            this.button_DeleteGrade.TabIndex = 20;
            this.button_DeleteGrade.Text = "Smazat Známku";
            this.button_DeleteGrade.UseVisualStyleBackColor = true;
            this.button_DeleteGrade.Click += new System.EventHandler(this.button_DeleteGrade_Click);
            // 
            // StudentForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(724, 223);
            this.Controls.Add(this.button_DeleteGrade);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.comboBox_FOS);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.textBox_year);
            this.Controls.Add(this.textBox_PIN);
            this.Controls.Add(this.textBox_sname);
            this.Controls.Add(this.textBox_name);
            this.Controls.Add(this.textBox_id);
            this.Controls.Add(this.label_year);
            this.Controls.Add(this.label_FOS);
            this.Controls.Add(this.label_PIN);
            this.Controls.Add(this.label_sname);
            this.Controls.Add(this.label_birth);
            this.Controls.Add(this.label_id);
            this.Controls.Add(this.label_name);
            this.Name = "StudentForm";
            this.Text = "Detail studenta";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label_name;
        private System.Windows.Forms.Label label_id;
        private System.Windows.Forms.Label label_birth;
        private System.Windows.Forms.Label label_sname;
        private System.Windows.Forms.Label label_PIN;
        private System.Windows.Forms.Label label_FOS;
        private System.Windows.Forms.Label label_year;
        private System.Windows.Forms.TextBox textBox_id;
        private System.Windows.Forms.TextBox textBox_name;
        private System.Windows.Forms.TextBox textBox_sname;
        private System.Windows.Forms.TextBox textBox_PIN;
        private System.Windows.Forms.TextBox textBox_year;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.DataGridViewTextBoxColumn clm_grade;
        private System.Windows.Forms.DataGridViewTextBoxColumn clm_ate;
        private System.Windows.Forms.DataGridViewTextBoxColumn clm_subject;
        private System.Windows.Forms.ComboBox comboBox_FOS;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Button button_DeleteGrade;
    }
}

