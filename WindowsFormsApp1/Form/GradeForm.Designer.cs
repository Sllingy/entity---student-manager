﻿namespace WindowsFormsApp1
{
    partial class GradeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.lbl_value = new System.Windows.Forms.Label();
            this.lbl_date = new System.Windows.Forms.Label();
            this.lbl_subject = new System.Windows.Forms.Label();
            this.lbl_theme = new System.Windows.Forms.Label();
            this.lbl_type = new System.Windows.Forms.Label();
            this.textBox_theme = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.comboBox_subject = new System.Windows.Forms.ComboBox();
            this.comboBox_type = new System.Windows.Forms.ComboBox();
            this.numeric_Grade = new System.Windows.Forms.NumericUpDown();
            this.button_Cancel = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_Grade)).BeginInit();
            this.SuspendLayout();
            // 
            // lbl_value
            // 
            this.lbl_value.AutoSize = true;
            this.lbl_value.Location = new System.Drawing.Point(12, 14);
            this.lbl_value.Name = "lbl_value";
            this.lbl_value.Size = new System.Drawing.Size(46, 13);
            this.lbl_value.TabIndex = 0;
            this.lbl_value.Text = "Známka";
            // 
            // lbl_date
            // 
            this.lbl_date.AutoSize = true;
            this.lbl_date.Location = new System.Drawing.Point(12, 41);
            this.lbl_date.Name = "lbl_date";
            this.lbl_date.Size = new System.Drawing.Size(38, 13);
            this.lbl_date.TabIndex = 1;
            this.lbl_date.Text = "Datum";
            // 
            // lbl_subject
            // 
            this.lbl_subject.AutoSize = true;
            this.lbl_subject.Location = new System.Drawing.Point(11, 67);
            this.lbl_subject.Name = "lbl_subject";
            this.lbl_subject.Size = new System.Drawing.Size(47, 13);
            this.lbl_subject.TabIndex = 2;
            this.lbl_subject.Text = "Předmět";
            // 
            // lbl_theme
            // 
            this.lbl_theme.AutoSize = true;
            this.lbl_theme.Location = new System.Drawing.Point(12, 94);
            this.lbl_theme.Name = "lbl_theme";
            this.lbl_theme.Size = new System.Drawing.Size(34, 13);
            this.lbl_theme.TabIndex = 3;
            this.lbl_theme.Text = "Téma";
            // 
            // lbl_type
            // 
            this.lbl_type.AutoSize = true;
            this.lbl_type.Location = new System.Drawing.Point(12, 120);
            this.lbl_type.Name = "lbl_type";
            this.lbl_type.Size = new System.Drawing.Size(30, 13);
            this.lbl_type.TabIndex = 4;
            this.lbl_type.Text = "Druh";
            // 
            // textBox_theme
            // 
            this.textBox_theme.Location = new System.Drawing.Point(75, 91);
            this.textBox_theme.Name = "textBox_theme";
            this.textBox_theme.Size = new System.Drawing.Size(184, 20);
            this.textBox_theme.TabIndex = 8;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(15, 144);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(118, 23);
            this.button1.TabIndex = 10;
            this.button1.Text = "Uložit";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(75, 38);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(184, 20);
            this.dateTimePicker1.TabIndex = 12;
            // 
            // comboBox_subject
            // 
            this.comboBox_subject.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_subject.FormattingEnabled = true;
            this.comboBox_subject.Location = new System.Drawing.Point(75, 64);
            this.comboBox_subject.Name = "comboBox_subject";
            this.comboBox_subject.Size = new System.Drawing.Size(184, 21);
            this.comboBox_subject.TabIndex = 13;
            // 
            // comboBox_type
            // 
            this.comboBox_type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_type.FormattingEnabled = true;
            this.comboBox_type.Location = new System.Drawing.Point(75, 117);
            this.comboBox_type.Name = "comboBox_type";
            this.comboBox_type.Size = new System.Drawing.Size(184, 21);
            this.comboBox_type.TabIndex = 14;
            // 
            // numeric_Grade
            // 
            this.numeric_Grade.Location = new System.Drawing.Point(75, 12);
            this.numeric_Grade.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numeric_Grade.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numeric_Grade.Name = "numeric_Grade";
            this.numeric_Grade.Size = new System.Drawing.Size(184, 20);
            this.numeric_Grade.TabIndex = 15;
            this.numeric_Grade.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // button_Cancel
            // 
            this.button_Cancel.Location = new System.Drawing.Point(141, 144);
            this.button_Cancel.Name = "button_Cancel";
            this.button_Cancel.Size = new System.Drawing.Size(118, 23);
            this.button_Cancel.TabIndex = 16;
            this.button_Cancel.Text = "Zrušit";
            this.button_Cancel.UseVisualStyleBackColor = true;
            this.button_Cancel.Click += new System.EventHandler(this.button_Cancel_Click);
            // 
            // GradeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(272, 180);
            this.Controls.Add(this.button_Cancel);
            this.Controls.Add(this.numeric_Grade);
            this.Controls.Add(this.comboBox_type);
            this.Controls.Add(this.comboBox_subject);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.textBox_theme);
            this.Controls.Add(this.lbl_type);
            this.Controls.Add(this.lbl_theme);
            this.Controls.Add(this.lbl_subject);
            this.Controls.Add(this.lbl_date);
            this.Controls.Add(this.lbl_value);
            this.Name = "GradeForm";
            this.Text = "Detail Známky";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.GradeForm_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.numeric_Grade)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_value;
        private System.Windows.Forms.Label lbl_date;
        private System.Windows.Forms.Label lbl_subject;
        private System.Windows.Forms.Label lbl_theme;
        private System.Windows.Forms.Label lbl_type;
        private System.Windows.Forms.TextBox textBox_theme;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.ComboBox comboBox_subject;
        private System.Windows.Forms.ComboBox comboBox_type;
        private System.Windows.Forms.NumericUpDown numeric_Grade;
        private System.Windows.Forms.Button button_Cancel;
    }
}