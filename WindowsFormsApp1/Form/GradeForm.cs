﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class GradeForm : Form
    {
        StudentForm parrentForm;
        Grade grade;
        
        public GradeForm(StudentForm form, Grade grade) {            
            parrentForm = form;
            this.grade = grade;

            Init();
            LoadData();
        }

        public GradeForm(StudentForm form) {
            parrentForm = form;

            Init();
        }

        private void Init() {
            FormBorderStyle = FormBorderStyle.FixedSingle;

            InitializeComponent();
            AddTypes();
            AddSubjects();            
        }

        private void AddTypes() {
            comboBox_type.Items.Add("M");
            comboBox_type.Items.Add("V");
            comboBox_type.Items.Add("Z");
            comboBox_type.Items.Add("O");
            comboBox_type.Items.Add("U");
            comboBox_type.Items.Add("H");
        }

        private void AddSubjects() {
            comboBox_subject.Items.Add("MAT");
            comboBox_subject.Items.Add("CJL");
            comboBox_subject.Items.Add("ANJ");
            comboBox_subject.Items.Add("PRG");
            comboBox_subject.Items.Add("POS");
            comboBox_subject.Items.Add("GRS");
            comboBox_subject.Items.Add("PVY");
        }

        public void LoadData() {                
            numeric_Grade.Value = grade.Value;
            dateTimePicker1.Value = grade.Date;
            comboBox_subject.Text = grade.Subject;
            textBox_theme.Text = grade.Theme;
            comboBox_type.Text = grade.Type;
        }

        private void GradeForm_FormClosing(object sender, FormClosingEventArgs e) {
            parrentForm.ReloadGrid();
            parrentForm.Show();
        }

        private void button1_Click(object sender, EventArgs e) {
            if(comboBox_subject.Text == "" || textBox_theme.Text == "" || comboBox_type.Text == "") {
                MessageBox.Show("Žádná hodnota nesmí být prázdná", "Ověření");
            } else {
                if (this.grade != null) {
                    grade.Value = (int)numeric_Grade.Value;
                    grade.Date = dateTimePicker1.Value;
                    grade.Subject = comboBox_subject.Text;
                    grade.Theme = textBox_theme.Text;
                    grade.Type = comboBox_type.Text;
                } else {
                    Grade g = new Grade();
                    parrentForm.grades.Add(g);

                    g.Value = (int)numeric_Grade.Value;
                    g.Date = dateTimePicker1.Value;
                    g.Subject = comboBox_subject.Text;
                    g.Theme = textBox_theme.Text;
                    g.Type = comboBox_type.Text;
                }
                this.Close();
            }            
        }

        private void button_Cancel_Click(object sender, EventArgs e) {
            this.Close();
        }
    }
}
