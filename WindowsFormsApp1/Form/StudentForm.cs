﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class StudentForm : Form
    {
        Student student;
        StudentGrid parrentForm;
        SchoolContext context;
        public List<Grade> grades = new List<Grade>();

        public StudentForm(SchoolContext context, StudentGrid form) {
            this.context = context;
            parrentForm = form;       
            
            Init();
        }    
        
        public StudentForm(SchoolContext context,  StudentGrid form, Student student) {
            this.context = context;
            parrentForm = form;
            this.student = student;

            Init();
            LoadData();
            ReloadGrid();
        }

        private void Init() {
            FormBorderStyle = FormBorderStyle.FixedSingle;
            InitializeComponent();
            AddFieldOfStudies();            
        }

        private void AddFieldOfStudies() {
            comboBox_FOS.Items.Add("Programování");
            comboBox_FOS.Items.Add("Grafika");
            comboBox_FOS.Items.Add("Počítačové sítě");
        }

        public void LoadData() {                
            textBox_id.Text = student.Id.ToString();
            textBox_name.Text = student.Name;
            textBox_sname.Text = student.Surname;
            textBox_year.Text = student.Year.ToString();
            dateTimePicker1.Value = student.DateOfBirth;
            textBox_PIN.Text = student.Pin;
            comboBox_FOS.Text = student.FieldOfStudy;

            grades = student.Grades.ToList();
        }

        public void ReloadGrid() {
            dataGridView1.Rows.Clear();
            
            foreach (Grade grade in grades) {
                object[] row = { grade.Value, grade.Date, grade.Subject };
                this.dataGridView1.Rows.Add(row);
            }            
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e) {
            parrentForm.ReloadGrid();
            parrentForm.Show();
        }
   

        private void button2_Click(object sender, EventArgs e) {
            DialogResult confirmResult = MessageBox.Show("Opravdu chcete smazat studenta?","Potrvďte smazání", MessageBoxButtons.YesNo);
            if (confirmResult == DialogResult.Yes) {
                if(student != null) {
                    context.Students.Remove(student);
                    context.SaveChanges();
                }
                this.Close();
            }
        }

        private void button3_Click(object sender, EventArgs e) {                        
            GradeForm form = new GradeForm(this);

            this.Hide();
            form.ShowDialog();
        }

        private void button_DeleteGrade_Click(object sender, EventArgs e) {
            if (dataGridView1.CurrentCell != null) {
                DialogResult confirmResult = MessageBox.Show("Opravdu chcete smazat známku?", "Potrvďte smazání", MessageBoxButtons.YesNo);
                if (confirmResult == DialogResult.Yes) {                                       
                    Grade grade = grades[dataGridView1.CurrentCell.RowIndex];
                    grades.Remove(grade);
                    ReloadGrid();
                }
            }            
        }

        private void dataGridView1_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e) {
            if (dataGridView1.CurrentCell != null && e.RowIndex != -1) {                
                GradeForm form = new GradeForm(this, student.Grades[dataGridView1.CurrentCell.RowIndex]);

                this.Hide();
                form.ShowDialog();
            }
        }

        private void button__OK_Click(object sender, EventArgs e) {
            if (textBox_name.Text == "" || textBox_sname.Text == "" || textBox_year.Text == "" || textBox_PIN.Text == "" || comboBox_FOS.Text == "") {
                MessageBox.Show("Žádná hodnota nesmí být prázdná", "Ověření");
            } else {
                int studentId;
                if(student != null) {
                    student.Name = new string(textBox_name.Text.Where(char.IsLetter).ToArray());
                    student.Surname = new string(textBox_sname.Text.Where(char.IsLetter).ToArray());
                    Int32.TryParse(textBox_year.Text, out int year);
                    student.Year = year;
                    student.DateOfBirth = dateTimePicker1.Value;
                    student.Pin = textBox_PIN.Text;
                    student.FieldOfStudy = comboBox_FOS.Text;
                    student.Grades = grades;
                    studentId = student.Id;
                } else {
                    Student s = new Student();
                    context.Students.Add(s);

                    s.Name = new string(textBox_name.Text.Where(char.IsLetter).ToArray());
                    s.Surname = new string(textBox_sname.Text.Where(char.IsLetter).ToArray());
                    Int32.TryParse(textBox_year.Text, out int year);
                    s.Year = year;
                    s.DateOfBirth = dateTimePicker1.Value;
                    s.Pin = textBox_PIN.Text;
                    s.FieldOfStudy = comboBox_FOS.Text;
                    s.Grades = grades;
                    studentId = s.Id;
                }                
                
                List<Grade> studentGrades = context.Grades.Where(g => g.StudentId == studentId).ToList();

                foreach (Grade grade in grades) {
                    studentGrades.Remove(studentGrades.Where(g => g.Id == grade.Id).FirstOrDefault());
                }

                foreach (Grade grade in studentGrades) {
                    context.Grades.Remove(grade);
                }
                
                context.SaveChanges();
                this.Close();
            }
        }        

        private void button_Cancel_Click_1(object sender, EventArgs e) {
            this.Close();
        }
    }
}
