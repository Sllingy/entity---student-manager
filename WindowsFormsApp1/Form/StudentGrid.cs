﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class StudentGrid : Form
    {        
        SchoolContext context = new SchoolContext();

        public StudentGrid() {
            FormBorderStyle = FormBorderStyle.FixedSingle;
            InitializeComponent();            
        }

        private void Form2_Load(object sender, EventArgs e) {
            ReloadGrid();
        }        

        public void ReloadGrid() {
            dataGridView2.Rows.Clear();

            foreach (var student in context.Students.ToList()) {
                string[] row = { student.Id.ToString(), student.Name, student.Surname, student.DateOfBirth.ToString() };
                this.dataGridView2.Rows.Add(row);
            }
        }

        private void button1_Click(object sender, EventArgs e) {
            this.Hide();

            StudentForm form = new StudentForm(context, this);
            form.ShowDialog();
        }        

        private void dataGridView2_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e) {
            if (dataGridView2.CurrentCell != null && e.RowIndex != -1) {
                this.Hide();
                StudentForm form = new StudentForm(context, this, context.Students.ToList()[dataGridView2.CurrentCell.RowIndex]);
                form.ShowDialog();
            }
        }

        private void button_StudentDelete_Click(object sender, EventArgs e) {
            DialogResult confirmResult = MessageBox.Show("Opravdu chcete smazat studenta?", "Potrvďte smazání", MessageBoxButtons.YesNo);
            if (confirmResult == DialogResult.Yes) {
                context.Students.Remove(context.Students.ToList()[dataGridView2.CurrentCell.RowIndex]);
                context.SaveChanges();
                ReloadGrid();
            }
        }
    }
}
