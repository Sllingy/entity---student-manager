﻿namespace WindowsFormsApp1
{
    partial class StudentGrid
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.clm_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clm_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clm_sname = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clm_birth = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.button1 = new System.Windows.Forms.Button();
            this.button_StudentDelete = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.AllowUserToDeleteRows = false;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.clm_id,
            this.clm_name,
            this.clm_sname,
            this.clm_birth});
            this.dataGridView2.Location = new System.Drawing.Point(12, 12);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.ReadOnly = true;
            this.dataGridView2.Size = new System.Drawing.Size(463, 298);
            this.dataGridView2.TabIndex = 1;
            this.dataGridView2.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView2_CellContentDoubleClick);
            // 
            // clm_id
            // 
            this.clm_id.HeaderText = "ID";
            this.clm_id.Name = "clm_id";
            this.clm_id.ReadOnly = true;
            this.clm_id.Width = 30;
            // 
            // clm_name
            // 
            this.clm_name.HeaderText = "Jméno";
            this.clm_name.Name = "clm_name";
            this.clm_name.ReadOnly = true;
            this.clm_name.Width = 130;
            // 
            // clm_sname
            // 
            this.clm_sname.HeaderText = "Příjmení";
            this.clm_sname.Name = "clm_sname";
            this.clm_sname.ReadOnly = true;
            this.clm_sname.Width = 130;
            // 
            // clm_birth
            // 
            this.clm_birth.HeaderText = "Datum Narození";
            this.clm_birth.Name = "clm_birth";
            this.clm_birth.ReadOnly = true;
            this.clm_birth.Width = 130;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 316);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(225, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "Přidat Studenta";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button_StudentDelete
            // 
            this.button_StudentDelete.Location = new System.Drawing.Point(250, 316);
            this.button_StudentDelete.Name = "button_StudentDelete";
            this.button_StudentDelete.Size = new System.Drawing.Size(225, 23);
            this.button_StudentDelete.TabIndex = 3;
            this.button_StudentDelete.Text = "Smazat Studenta";
            this.button_StudentDelete.UseVisualStyleBackColor = true;
            this.button_StudentDelete.Click += new System.EventHandler(this.button_StudentDelete_Click);
            // 
            // StudentGrid
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(489, 350);
            this.Controls.Add(this.button_StudentDelete);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.dataGridView2);
            this.Name = "StudentGrid";
            this.Text = "Seznam studentů";
            this.Load += new System.EventHandler(this.Form2_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button_StudentDelete;
        private System.Windows.Forms.DataGridViewTextBoxColumn clm_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn clm_name;
        private System.Windows.Forms.DataGridViewTextBoxColumn clm_sname;
        private System.Windows.Forms.DataGridViewTextBoxColumn clm_birth;
    }
}