﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    class GradeModel
    {
        private StudentContext context = new StudentContext();

        public List<Grade> GetGrades() {
            return context.Grades.ToList();
        }

        public void Create(Grade grade) {
            context.Grades.Add(grade);
            context.SaveChanges();
        }

        public Grade GetGradeLast() {
            return context.Grades.ToList().Last();
        }

        public void SaveChanges() {
            context.SaveChanges();
        }

        public void Delete(Grade grade) {
            context.Grades.Remove(grade);
        }        
    }
}
